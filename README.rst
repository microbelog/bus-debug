MicrobeLog bus debug
====================

Simple monitoring and injection for the MicrobeLog pluggable bus.

Reads lines from stdin and puts them on the bus. Reads messages from the bus
and puts them on stdout. Includes the simplest possible bus that simply runs
in process and simply provides a loopback queue.

This service also serves as a prototype and example for how to write a
producer/consumer for the pluggable bus.
