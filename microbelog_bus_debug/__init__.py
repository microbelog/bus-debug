#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import asyncio
import signal
import functools
import sys

loop = asyncio.get_event_loop()

def abort(signame):
    print("got signal %s: exit" % signame)
    loop.stop()

for signame in ('SIGINT', 'SIGTERM'):
    loop.add_signal_handler(getattr(signal, signame),
                            functools.partial(abort, signame))

class EchoBus():
    def __init__(self):
        self._queue = asyncio.Queue()

    @asyncio.coroutine
    def send(self, msg):
        print("putting message: {}".format(msg))
        yield from self._queue.put(msg)

    @asyncio.coroutine
    def recv(self):
        msg = yield from self._queue.get()
        #print("received message: {}".format(msg))
        return msg

def create_bus():
    return EchoBus()

class StdinProtocol(asyncio.Protocol):
    def __init__(self, bus):
        self._buffer = bytearray()
        self.bus = bus

    def send_buffer(self):
        if len(self._buffer) > 0:
            asyncio.async(self.bus.send(bytes(self._buffer)))

        self._buffer.clear()

    def data_received(self, data):
        for c in data:
            if c == 10:
                self.send_buffer()
            else:
                self._buffer.append(c)

    def eof_received(self):
        self.send_buffer()
        abort('EOF')


@asyncio.coroutine
def go(bus):
    yield from loop.connect_read_pipe(functools.partial(StdinProtocol, bus),
                                      sys.stdin)
    while True:
        msg = yield from bus.recv()
        print(msg)

def main():
    bus = create_bus()
    asyncio.async(go(bus))
    loop.run_forever()

if __name__ == '__main__':
    main()
