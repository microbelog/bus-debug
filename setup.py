import os
import sys

from setuptools import setup, find_packages

here = os.path.abspath(os.path.dirname(__file__))
NAME = 'microbelog_bus_debug'
with open(os.path.join(here, 'README.rst')) as readme:
    README = readme.read()
with open(os.path.join(here, 'CHANGES.rst')) as changes:
    CHANGES = changes.read()

requires = [
    'asyncio'
]

setup(
    name=NAME,
    version='0.1.0',
    description='Simple monitoring and injection for the MicrobeLog pluggable bus',
    long_description=README + '\n\n' + CHANGES,
    classifiers=[
        "Programming Language :: Python",
        "Programming Language :: Python :: 3.3",
        "Programming Language :: Python :: 3.4",
        "Topic :: Internet",
    ],
    author='Claes Wallin',
    author_email='claes.wallin@greatsinodevelopment.com',
    url='https://gitlab.com/microbelog/manifest',
    keywords='asyncio messaging bus',
    packages=find_packages(),
    include_package_data=True,
    zip_safe=True,
    test_suite=NAME,
    install_requires=requires,
)
